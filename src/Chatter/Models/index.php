<?php
require "bootstrap.php";
use Chatter\Models\Message;

$app = new \Slim\App();
$app->get('/hello/{name}', function($request, $response,$args){
   return $response->write('Hello '.$args['name']);
});

$app->get('/customers/{cnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber']);
});

$app->get('/customers/{cnumber}/products/{pnumber}', function($request, $response,$args){
   return $response->write('Customer '.$args['cnumber' ]. ' Product '.$args['pnumber' ]);
});

$app->get('/messages', function($request, $response,$args){
   $_message = new Message();
   $messages = $_message->all();
   $payload = [];
   foreach($messages as $msg){
        $payload[$msg->id] = [
            'body'=>$msg->body,
            'user_id'=>$msg->user_id,
            'created_at'=>$msg->created_at
        ];
   }
   return $response->withStatus(200)->withJson($payload);
});

$app->post('/messages', function($request, $response,$args){
    $message = $request->getParsedBodyParam('message','');
    $userid =$request->getParsedBodyParam('userid','');
    $_message = new Message();
    $_message->body =$message;
    $_message->user_id = $userid;
    $_message->save();
    if($_message->id){
        $payload = ['message_id'=>$_message->id];
        return $response->withStatus(201)->withJson($payload);
    }else{
        return $response->withStatus(400);
    }

    

});

$app->delete('/messages/{message_id}', function($request, $response,$args){
    $message = Message::find($args['message_id']);
    $message->delete();
    if($message->exists){
        return $response->withStatus(400);
    }else{
        return $response->withStatus(200);
    }

    

});

$app->run();